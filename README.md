# meteor-typescript-jest

A boilerplate configured with Typescript and Jest

# what is included 
- Meteor
- Typescript
- React
- Jest
- Webpack

All the above are pre-configured

# Development and usage

if you don't have meteor run the script below  

`curl https://install.meteor.com/ | sh`

after that clone the repo  

`git clone https://github.com/OlivierJM/meteor-typescript-jest-boilerplate.git MyProject`  

`cd MyProject`  

install dependencies  

`meteor npm install`   

run it  

`meteor` or `npm run start`  

# Testing  

`npm run test`  

# To-do  
- [ ] Make a branch that just uses Javascript not Typescript  
- [ ] Make a branch that uses meteor's default bundler   


